package fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Objects;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import pegafrica.com.go_mobile.R;
import util.FontCache;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class MyProfileFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private ImageView profileImageView;
    Typeface typeface, typefaceBold;
    private TextView nameTextView, emailTextView, phoneTextView;
    static final Integer CAMERA = 0x5;
    File imageFile;
    String imagePath = "";
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    CircleImageView circleImageView;
    Uri imageUri;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static MyProfileFragment newInstance(String param1, String param2) {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MyProfileFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @SuppressLint("CommitPrefEdits")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        verifyStoragePermissions(getActivity());
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        nameTextView = view.findViewById(R.id.dsr_name);
        emailTextView = view.findViewById(R.id.dsr_email);
        phoneTextView = view.findViewById(R.id.dsr_phone);
        circleImageView = view.findViewById(R.id.user_avatar);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        edit = prefs.edit();
        setUserAvatar(view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        typeface = FontCache.get("Lato-Regular.ttf", getActivity());
        typefaceBold = FontCache.get("Lato-Bold.ttf", getActivity());
        nameTextView.setTypeface(typefaceBold);
        emailTextView.setTypeface(typefaceBold);
        phoneTextView.setTypeface(typefaceBold);
        circleImageView.setOnClickListener(view1 -> selectImage());
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    private void selectImage() {
        final CharSequence[] options = {"Capture Image", "Upload from Gallery"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Capture Image")) {
                askForPermission(Manifest.permission.CAMERA, CAMERA);
            } else if (options[item].equals("Upload from Gallery")) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 2);
            }
        });
        builder.show();
    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
            }
        } else {
            StrictMode.VmPolicy.Builder builder2 = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder2.build());
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                File exportDir = new File(Environment.getExternalStorageDirectory(), "TempFolder");
                if (!exportDir.exists()) {
                    exportDir.mkdirs();
                } else {
                    exportDir.delete();
                }
                imageFile = new File(exportDir, "/" + UUID.randomUUID().toString().replaceAll("-", "") + ".jpg");
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
                startActivityForResult(cameraIntent, 1);
            }
        }
    }


    public void setUserAvatar(View view) {
        File imgFile = new File(prefs.getString("image_string", ""));
        if (imgFile.exists()) {
            CircleImageView myImage = view.findViewById(R.id.user_avatar);
            setImageWithPicasso(imgFile, myImage);
        }
    }

    public void setImageWithPicasso(File file, CircleImageView circleImageView) {
        Picasso.with(getActivity()).load(file).into(circleImageView);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            imagePath = imageFile.getPath();
            Log.d("DSR imagePath", imagePath);
            setImageWithPicasso(new File(imagePath), circleImageView);
            edit.putString("image_string", imagePath);
            edit.commit();
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Toast.makeText(getActivity(), "Image not selected", Toast.LENGTH_LONG).show();
                return;
            } else {
                imageUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = Objects.requireNonNull(getActivity()).getContentResolver().query(imageUri, filePath, null, null, null);
                assert c != null;
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                if (!imagePath.isEmpty() && new File(imagePath).exists()) {
                    new File(imagePath).delete();
                }
                imagePath = c.getString(columnIndex);
                c.close();
                setImageWithPicasso(new File(imagePath), circleImageView);
                edit.putString("image_string", imagePath);
                edit.commit();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public static void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


}
