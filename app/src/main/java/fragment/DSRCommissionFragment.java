package fragment;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pegafrica.com.go_mobile.R;
import util.FontCache;

public class DSRCommissionFragment extends Fragment {

    private DSRCommissionFragment.OnFragmentInteractionListener mListener;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    TextView progressiveCommissionHeader, progressiveCommissionValue, salesHeaderTextView,
            shsHeader, shsValue, tvHeader, tvValue, shsProgressiveHeader, shsProgressiveValue,
            tvProgressiveHeader, tvProgressiveValue, shsMidHeader, shsMidValue, tvMidMonthHeader,
            tvMidMonthValue, midAmountHeader, midAmountValue, staffDeductionHeader, staffDeductionValue,
            shsPayoutHeader, shsPayoutValue, tvPayoutHeader, tvPayoutValue, halfPayout, halfValue;
    Typeface typeface, typefaceBold;


    public static DSRCommissionFragment newInstance(String param1, String param2) {
        DSRCommissionFragment fragment = new DSRCommissionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public DSRCommissionFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dsr_commission_fragment, container, false);
        progressiveCommissionHeader = view.findViewById(R.id.progressive_commission_header);
        progressiveCommissionValue = view.findViewById(R.id.progressive_commission_value);
        salesHeaderTextView = view.findViewById(R.id.sales);
        shsHeader = view.findViewById(R.id.shs_header);
        shsValue = view.findViewById(R.id.shs_value);
        tvHeader = view.findViewById(R.id.tv_header);
        tvValue = view.findViewById(R.id.tv_value);
        shsProgressiveHeader = view.findViewById(R.id.progression_shs);
        shsProgressiveValue = view.findViewById(R.id.progression_shs_value);
        tvProgressiveHeader = view.findViewById(R.id.progression_tv);
        tvProgressiveValue = view.findViewById(R.id.progression_tv_value);
        shsMidHeader = view.findViewById(R.id.shs_mid_month);
        shsMidValue = view.findViewById(R.id.shs_mid_month_value);
        tvMidMonthHeader = view.findViewById(R.id.tv_mid_month);
        tvMidMonthValue = view.findViewById(R.id.tv_mid_month_value);
        midAmountHeader = view.findViewById(R.id.mid_month_amount);
        midAmountValue = view.findViewById(R.id.mid_month_value);
        staffDeductionHeader = view.findViewById(R.id.staff_device_deduction);
        staffDeductionValue = view.findViewById(R.id.device_deduction_value);
        shsPayoutHeader = view.findViewById(R.id.shs_payout);
        shsPayoutValue = view.findViewById(R.id.shs_payout_value);
        tvPayoutHeader = view.findViewById(R.id.tv_payout);
        tvPayoutValue = view.findViewById(R.id.tv_payout_value);
        halfPayout = view.findViewById(R.id.half_payout);
        halfValue = view.findViewById(R.id.half_payout_value);


        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        typeface = FontCache.get("Lato-Regular.ttf", getActivity());
        typefaceBold = FontCache.get("Lato-Bold.ttf", getActivity());
        progressiveCommissionHeader.setTypeface(typefaceBold);
        progressiveCommissionValue.setTypeface(typeface);
        salesHeaderTextView.setTypeface(typefaceBold);
        shsHeader.setTypeface(typefaceBold);
        shsValue.setTypeface(typeface);
        tvHeader.setTypeface(typefaceBold);
        tvValue.setTypeface(typeface);
        shsProgressiveHeader.setTypeface(typefaceBold);
        shsProgressiveValue.setTypeface(typeface);
        tvProgressiveHeader.setTypeface(typefaceBold);
        tvProgressiveValue.setTypeface(typeface);
        shsMidHeader.setTypeface(typefaceBold);
        shsMidValue.setTypeface(typeface);
        tvMidMonthHeader.setTypeface(typefaceBold);
        tvMidMonthValue.setTypeface(typeface);
        midAmountHeader.setTypeface(typefaceBold);
        midAmountValue.setTypeface(typeface);
        staffDeductionHeader.setTypeface(typefaceBold);
        staffDeductionValue.setTypeface(typeface);
        shsPayoutHeader.setTypeface(typefaceBold);
        shsPayoutValue.setTypeface(typeface);
        tvPayoutHeader.setTypeface(typefaceBold);
        tvPayoutValue.setTypeface(typeface);
        halfPayout.setTypeface(typefaceBold);
        halfValue.setTypeface(typeface);
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

}
