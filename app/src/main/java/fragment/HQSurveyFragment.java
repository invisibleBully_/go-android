package fragment;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import pegafrica.com.go_mobile.R;
import util.FontCache;

public class HQSurveyFragment extends Fragment {

    private HQSurveyFragment.OnFragmentInteractionListener mListener;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    Typeface typefaceBold;
    private TextView sfmUpdate, sfmCommission, sfmComments;
    private Button sfmUpdateYes, sfmUpdateNotSure, sfmUpdateNo, sfmCommissionYes, sfmCommissionNotSure, sfmCommissionNo, sfmCommentsYes, sfmCommentsUnsure, sfmCommentsNo;


    public static HQSurveyFragment newInstance(String param1, String param2) {
        HQSurveyFragment fragment = new HQSurveyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public HQSurveyFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hq_survey_fragment, container, false);
        sfmUpdate = view.findViewById(R.id.info_update);
        sfmCommission = view.findViewById(R.id.sfm_commission);
        sfmComments = view.findViewById(R.id.sfm_comment);
        sfmUpdateYes = view.findViewById(R.id.info_yes);
        sfmUpdateNo = view.findViewById(R.id.info_no);
        sfmUpdateNotSure = view.findViewById(R.id.info_not_sure);
        sfmCommentsYes = view.findViewById(R.id.comment_yes);
        sfmCommentsNo = view.findViewById(R.id.comment_no);
        sfmCommentsUnsure = view.findViewById(R.id.comment_unsure);
        sfmCommissionYes = view.findViewById(R.id.commission_yes);
        sfmCommissionNo = view.findViewById(R.id.commission_no);
        sfmCommissionNotSure = view.findViewById(R.id.commission_not_sure);


        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        typefaceBold = FontCache.get("Lato-Bold.ttf", getActivity());

        sfmUpdate.setTypeface(typefaceBold);
        sfmCommission.setTypeface(typefaceBold);
        sfmComments.setTypeface(typefaceBold);
        sfmUpdateYes.setTypeface(typefaceBold);
        sfmUpdateNo.setTypeface(typefaceBold);
        sfmUpdateNotSure.setTypeface(typefaceBold);
        sfmCommentsYes.setTypeface(typefaceBold);
        sfmCommentsNo.setTypeface(typefaceBold);
        sfmCommentsUnsure.setTypeface(typefaceBold);
        sfmCommissionYes.setTypeface(typefaceBold);
        sfmCommissionNo.setTypeface(typefaceBold);
        sfmCommissionNotSure.setTypeface(typefaceBold);
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


}
