package activity;

import android.app.Dialog;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import pegafrica.com.go_mobile.R;
import util.FontCache;

public class AddNewCustomerActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener {


    private TextView genderLabel, locationPictureTextView, idPictureTextView;
    private AlertDialog alertDialog;
    private EditText firstNameEditText, lastNameEditText, dateOfBirthEditText, occupationEditText,
            languagesEditText, idTypeEditText, regionEditText, paymentPlanEditText, unitAccountNumberEditText,
            idNumberEditText, primaryPhoneNumberEditText, alternatePhoneNumberEditText, nextOfKinName, nextOfKinNumber,
            customerLocation, townVillage;
    private ImageView locationPicture, IdPicture;

    private Button genderMale, genderFemale, cancelButton, saveButton;
    String selectedGender = "";
    final ArrayList<LanguageVO> languageList = new ArrayList<>();
    private Toolbar toolbar;
    String date;


    final String[] languages = new String[]{
            "Ga",
            "Twi",
            "Ewe",
            "Nzema",
            "Dagbani"
    };


    final boolean[] checkedLanguages = new boolean[]{
            false,
            false,
            false,
            false,
            false
    };


    private CharSequence[] idValues = {"Drivers' License", "Voter ID", "Passport", "NHIS", "National ID"};
    private CharSequence[] regionalValues = {"Ashanti", "Brong-Ahafo", "Greater Accra", "Central", "Eastern", "Western", "Volta", "Upper-East", "Upper-West", "Northern"};
    private CharSequence[] paymentPlanValues = {"D30 Daily Rate", "D30 Monthly Rate",
            "New Customer X850 Monthly", "New Customer X850 Weekly",
            "Old Customer X850 Monthly", "Old Customer X850 Weekly", "Cocoa D30", "GOVCustomer D30 Monthly"};


    String selectedLanguages, selectedIdValue, selectedRegionValue, selectedPaymentPlanValue = "";
    final ArrayList languagesSelected = new ArrayList();
    private Typeface typeface, typefaceBold;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_customer);
        registerComponents();
    }


    private void registerComponents() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);

            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayUseLogoEnabled(false);
                actionBar.setHomeButtonEnabled(true);
            }
        }

        typeface = FontCache.get("Lato-Regular.ttf", AddNewCustomerActivity.this);
        typefaceBold = FontCache.get("Lato-Bold.ttf", AddNewCustomerActivity.this);


        firstNameEditText = findViewById(R.id.first_name);
        lastNameEditText = findViewById(R.id.last_name);
        dateOfBirthEditText = findViewById(R.id.date_of_birth);
        genderLabel = findViewById(R.id.gender_label);
        genderMale = findViewById(R.id.male);
        genderFemale = findViewById(R.id.female);
        occupationEditText = findViewById(R.id.occupation);
        primaryPhoneNumberEditText = findViewById(R.id.primary_phone_number);
        alternatePhoneNumberEditText = findViewById(R.id.alternate_phone_number);
        nextOfKinName = findViewById(R.id.next_of_kin_name);
        nextOfKinNumber = findViewById(R.id.next_of_kin_number);
        customerLocation = findViewById(R.id.customer_location);
        townVillage = findViewById(R.id.town_village);
        languagesEditText = findViewById(R.id.language);
        idTypeEditText = findViewById(R.id.id_type);
        regionEditText = findViewById(R.id.region);
        idNumberEditText = findViewById(R.id.id_number);
        paymentPlanEditText = findViewById(R.id.payment_plan);
        unitAccountNumberEditText = findViewById(R.id.unit_account_number);
        locationPicture = findViewById(R.id.location_picture);
        IdPicture = findViewById(R.id.id_picture);
        locationPictureTextView = findViewById(R.id.location_picture_text_view);
        idPictureTextView = findViewById(R.id.id_picture_text_view);
        cancelButton = findViewById(R.id.cancel);
        saveButton = findViewById(R.id.save);

        firstNameEditText.setTypeface(typeface);
        lastNameEditText.setTypeface(typeface);
        dateOfBirthEditText.setTypeface(typeface);
        genderLabel.setTypeface(typefaceBold);
        genderMale.setTypeface(typefaceBold);
        genderFemale.setTypeface(typefaceBold);
        occupationEditText.setTypeface(typeface);
        primaryPhoneNumberEditText.setTypeface(typeface);
        alternatePhoneNumberEditText.setTypeface(typeface);
        nextOfKinName.setTypeface(typeface);
        nextOfKinNumber.setTypeface(typeface);
        customerLocation.setTypeface(typeface);
        townVillage.setTypeface(typeface);
        languagesEditText.setTypeface(typeface);
        idTypeEditText.setTypeface(typeface);
        regionEditText.setTypeface(typeface);
        idNumberEditText.setTypeface(typeface);
        paymentPlanEditText.setTypeface(typeface);
        unitAccountNumberEditText.setTypeface(typeface);
        locationPictureTextView.setTypeface(typefaceBold);
        idPictureTextView.setTypeface(typefaceBold);
        cancelButton.setTypeface(typefaceBold);
        saveButton.setTypeface(typefaceBold);


        dateOfBirthEditText.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(AddNewCustomerActivity.this,
                    now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
            dpd.setVersion(DatePickerDialog.Version.VERSION_2);
            dpd.setAccentColor(getResources().getColor(android.R.color.holo_orange_dark));
            dpd.setOnCancelListener(dialogInterface -> dpd.dismiss());
            dpd.show(getFragmentManager(), "Pick Follow Up Date");
        });

        paymentPlanEditText.setOnClickListener(view -> createPaymentPlanAlertDialog());

        regionEditText.setOnClickListener(view -> createRegionAlertDialog());

        idTypeEditText.setOnClickListener(view -> createIDAlertDialog());

        languagesEditText.setOnClickListener(view -> {
            languagesSelected.clear();
            selectedLanguages = "";
            createLanguageAlertDialog();
        });


        genderMale.setOnClickListener(view -> {
            selectedGender = "MALE";
            genderMale.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            genderMale.setTextColor(getResources().getColor(android.R.color.white));
            genderFemale.setTextColor(getResources().getColor(android.R.color.black));
            genderFemale.setBackgroundResource(android.R.drawable.btn_default);
        });


        genderFemale.setOnClickListener(view -> {
            selectedGender = "FEMALE";
            genderFemale.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            genderFemale.setTextColor(getResources().getColor(android.R.color.white));
            genderMale.setTextColor(getResources().getColor(android.R.color.black));
            genderMale.setBackgroundResource(android.R.drawable.btn_default);
        });


        saveButton.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(AddNewCustomerActivity.this);
            dialog.setContentView(R.layout.customer_addition_questionaire);

            TextView confirmationText = dialog.findViewById(R.id.confirm_text);

            TextView registerMoMo = dialog.findViewById(R.id.mobile_money_registration);
            TextView freeInsurance = dialog.findViewById(R.id.free_insurance);
            TextView payToGo = dialog.findViewById(R.id.pay_to_go);
            TextView markCalendar = dialog.findViewById(R.id.mark_calendar);
            TextView installDevice = dialog.findViewById(R.id.install_device);
            TextView signedContract = dialog.findViewById(R.id.signed_contract);
            TextView customerMoMo = dialog.findViewById(R.id.momo);

            Button registerYes = dialog.findViewById(R.id.momo_reg_yes);
            Button registerNo = dialog.findViewById(R.id.momo_reg_no);
            Button insuranceYes = dialog.findViewById(R.id.insurance_yes);
            Button insuranceNo = dialog.findViewById(R.id.insurance_no);
            Button payToGoYes = dialog.findViewById(R.id.pay_to_go_yes);
            Button payToGoNo = dialog.findViewById(R.id.pay_to_go_no);
            Button markCalendarYes = dialog.findViewById(R.id.calendar_yes);
            Button markCalendarNo = dialog.findViewById(R.id.calendar_no);
            Button installYes = dialog.findViewById(R.id.install_yes);
            Button installNo = dialog.findViewById(R.id.install_no);
            Button signedYes = dialog.findViewById(R.id.contract_yes);
            Button signedNo = dialog.findViewById(R.id.contract_no);
            Button customerMoMoMoYes = dialog.findViewById(R.id.momo_yes);
            Button customerMoMoMoNo = dialog.findViewById(R.id.momo_no);
            Button save = dialog.findViewById(R.id.save);
            Button cancel = dialog.findViewById(R.id.cancel);


            confirmationText.setTypeface(typefaceBold);
            registerMoMo.setTypeface(typeface);
            freeInsurance.setTypeface(typeface);
            payToGo.setTypeface(typeface);
            markCalendar.setTypeface(typeface);
            installDevice.setTypeface(typeface);
            signedContract.setTypeface(typeface);
            customerMoMo.setTypeface(typeface);
            registerNo.setTypeface(typefaceBold);
            registerYes.setTypeface(typefaceBold);
            insuranceYes.setTypeface(typefaceBold);
            insuranceNo.setTypeface(typefaceBold);
            payToGoYes.setTypeface(typefaceBold);
            payToGoNo.setTypeface(typefaceBold);
            markCalendarYes.setTypeface(typefaceBold);
            markCalendarNo.setTypeface(typefaceBold);
            installYes.setTypeface(typefaceBold);
            installNo.setTypeface(typefaceBold);
            signedYes.setTypeface(typefaceBold);
            signedNo.setTypeface(typefaceBold);
            customerMoMoMoYes.setTypeface(typefaceBold);
            customerMoMoMoNo.setTypeface(typefaceBold);
            save.setTypeface(typefaceBold);
            cancel.setTypeface(typefaceBold);


            save.setOnClickListener(v -> {
                dialog.dismiss();

            });

            cancel.setOnClickListener(v -> {
                dialog.dismiss();

            });
            dialog.show();
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void createLanguageAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNewCustomerActivity.this);

        for (int i = 0; i < languages.length; i++) {
            LanguageVO languageVO = new LanguageVO();
            languageVO.setName(languages[i]);
            languageVO.setSelected(checkedLanguages[i]);
            languageList.add(languageVO);
        }

        builder.setMultiChoiceItems(languages, checkedLanguages, (dialog, which, isChecked) -> {
            languageList.get(which).setSelected(isChecked);
        });


        builder.setCancelable(false);
        builder.setTitle("Language");
        builder.setPositiveButton("OK", (dialog, which) -> {

            ArrayList<LanguageVO> selectedList = new ArrayList<>();
            for (int i = 0; i < languageList.size(); i++) {
                LanguageVO languageVO = languageList.get(i);
                languages[i] = languageVO.getName();
                checkedLanguages[i] = languageVO.isSelected();
                if (languageVO.isSelected()) {
                    selectedList.add(languageVO);
                }
            }

            for (int i = 0; i < selectedList.size(); i++) {
                if (i != selectedList.size() - 1) {
                    System.out.println(selectedList.get(i).getName() + ",");
                    selectedLanguages = selectedLanguages + selectedList.get(i).getName() + " , ";
                } else {
                    System.out.println(selectedList.get(i).getName() + "");
                    selectedLanguages = selectedLanguages + selectedList.get(i).getName() + " ";
                }
            }
            languagesEditText.setText(selectedLanguages);
            languageList.clear();
        });


        builder.setNegativeButton("Cancel", (dialog, which) -> {
            languageList.clear();
            dialog.dismiss();
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void createRegionAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNewCustomerActivity.this);
        builder.setTitle("Region");
        builder.setSingleChoiceItems(regionalValues, -1, (dialog, item) -> {
            selectedRegionValue = regionalValues[item] + "";
            regionEditText.setText(selectedRegionValue);
            alertDialog.dismiss();
        });
        alertDialog = builder.create();
        alertDialog.show();
    }


    public void createIDAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNewCustomerActivity.this);
        builder.setTitle("ID Type");
        builder.setSingleChoiceItems(idValues, -1, (dialog, item) -> {
            selectedIdValue = idValues[item] + "";
            idTypeEditText.setText(selectedIdValue);
            alertDialog.dismiss();
        });
        alertDialog = builder.create();
        alertDialog.show();
    }


    public void createPaymentPlanAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNewCustomerActivity.this);
        builder.setTitle("ID Type");
        builder.setSingleChoiceItems(paymentPlanValues, -1, (dialog, item) -> {
            selectedPaymentPlanValue = paymentPlanValues[item] + "";
            paymentPlanEditText.setText(selectedPaymentPlanValue);
            alertDialog.dismiss();
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = +dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        dateOfBirthEditText.setText(date);
    }


    private class LanguageVO {
        private String name;
        private boolean selected;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }
    }
}
