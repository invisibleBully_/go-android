package activity;

import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import pegafrica.com.go_mobile.R;
import util.FontCache;

public class AddReportActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private Toolbar toolbar;
    private Typeface typeface, typefaceBold;
    private EditText activityDate, noPresentations, noProspects, noProspectSales, noFreshSales,
            sfmTNT, transportAmount, serviceCentre;

    private Button save, cancel;
    String activityDateValue = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_report);
        registerComponents();
    }


    private void registerComponents() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);

            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayUseLogoEnabled(false);
                actionBar.setHomeButtonEnabled(true);
            }
        }

        typeface = FontCache.get("Lato-Regular.ttf", AddReportActivity.this);
        typefaceBold = FontCache.get("Lato-Bold.ttf", AddReportActivity.this);


        save = findViewById(R.id.save_button);
        cancel = findViewById(R.id.cancel_button);

        activityDate = findViewById(R.id.activity_date);
        noPresentations = findViewById(R.id.no_presentations);
        noProspects = findViewById(R.id.no_prospects);
        noProspectSales = findViewById(R.id.prospect_sales);
        noFreshSales = findViewById(R.id.fresh_sales);
        sfmTNT = findViewById(R.id.tnt_sfm);
        transportAmount = findViewById(R.id.transport_amount);
        serviceCentre = findViewById(R.id.service_centre);




        activityDate.setTypeface(typeface);
        noPresentations.setTypeface(typeface);
        noProspects.setTypeface(typeface);
        noProspectSales.setTypeface(typeface);
        noFreshSales.setTypeface(typeface);
        sfmTNT.setTypeface(typeface);
        transportAmount.setTypeface(typeface);
        serviceCentre.setTypeface(typeface);
        activityDate.setTypeface(typeface);
        activityDate.setTypeface(typeface);

        save.setTypeface(typefaceBold);
        cancel.setTypeface(typefaceBold);

        activityDate.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(AddReportActivity.this,
                    now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
            dpd.setVersion(DatePickerDialog.Version.VERSION_2);
            dpd.setAccentColor(getResources().getColor(android.R.color.holo_orange_dark));
            dpd.setOnCancelListener(dialogInterface -> dpd.dismiss());
            dpd.show(getFragmentManager(), "Pick Follow Up Date");
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        activityDateValue = +dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        activityDate.setText(activityDateValue);
    }
}
