package activity;

import android.content.Context;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import pegafrica.com.go_mobile.R;
import util.FontCache;


import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class AddTraineeActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private Typeface typeface, typefaceBold;
    private AlertDialog alertDialog;
    private CharSequence[] regionalValues = {"Ashanti", "Brong-Ahafo", "Greater Accra", "Central", "Eastern", "Western", "Volta", "Upper-East", "Upper-West", "Northern"};
    private EditText firstName, lastName, phoneNumber, alternatePhoneNumber, region, followUpDate, notes, prospectLocation;
    private String selectedRegionValue = "";
    private Toolbar toolbar;
    private Button d30Button, tvButton, yesExperience, noExperience, saveButton, cancelButton;
    private TextView productTitleHeader, gpsHeader, locationTitle, locationValue, coordinates, experience;
    private String selectedProductButton, pegExperience = "";

    //private FusedLocationProviderClient fusedLocationProviderClient;
    String date;
    //public Location mLastLocation = null;
    //private AddressResultReceiver mResultReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trainee);
        requestPermission();
        registerComponents();
        //getFusedLocationCoords();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void registerComponents() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);

            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayUseLogoEnabled(false);
                actionBar.setHomeButtonEnabled(true);
            }
        }

        //fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        typeface = FontCache.get("Lato-Regular.ttf", AddTraineeActivity.this);
        typefaceBold = FontCache.get("Lato-Bold.ttf", AddTraineeActivity.this);
        //TextView toolbarText = toolbar.findViewById(R.id.page_title);
        //toolbarText.setTypeface(typefaceBold);
//        firstName = findViewById(R.id.first_name);
//        lastName = findViewById(R.id.last_name);
//        phoneNumber = findViewById(R.id.phone_number);
//        alternatePhoneNumber = findViewById(R.id.alternate_phone_number);
//        region = findViewById(R.id.region);
//        d30Button = findViewById(R.id.d30);
//        tvButton = findViewById(R.id.tv);
//        productTitleHeader = findViewById(R.id.interested_product_title);
//        gpsHeader = findViewById(R.id.gps_header);
//        locationTitle = findViewById(R.id.location_title);
//        locationValue = findViewById(R.id.location_name);
//        coordinates = findViewById(R.id.coordinates);
//        followUpDate = findViewById(R.id.follow_up_date);
//        notes = findViewById(R.id.notes);
//        prospectLocation = findViewById(R.id.prospect_location);
//        experience = findViewById(R.id.experience);
//        yesExperience = findViewById(R.id.peg_experience);
//        noExperience = findViewById(R.id.no_peg_experience);
        cancelButton = findViewById(R.id.cancel_button);
        saveButton = findViewById(R.id.save_button);

//
//        firstName.setTypeface(typeface);
//        lastName.setTypeface(typeface);
//        phoneNumber.setTypeface(typeface);
//        alternatePhoneNumber.setTypeface(typeface);
//        region.setTypeface(typeface);
//        d30Button.setTypeface(typefaceBold);
//        tvButton.setTypeface(typefaceBold);
//        productTitleHeader.setTypeface(typefaceBold);
//        gpsHeader.setTypeface(typefaceBold);
//        followUpDate.setTypeface(typeface);
//        notes.setTypeface(typeface);
//        locationTitle.setTypeface(typefaceBold);
//        locationValue.setTypeface(typeface);
//        coordinates.setTypeface(typeface);
//        yesExperience.setTypeface(typefaceBold);
//        noExperience.setTypeface(typefaceBold);
//        experience.setTypeface(typefaceBold);
//        prospectLocation.setTypeface(typeface);
//        cancelButton.setTypeface(typefaceBold);
//        saveButton.setTypeface(typefaceBold);
//
//
//        region.setOnClickListener(view -> createRegionAlertDialog());
//
//
//        d30Button.setOnClickListener(view -> {
//            selectedProductButton = "D30";
//            d30Button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//            d30Button.setTextColor(getResources().getColor(android.R.color.white));
//            tvButton.setTextColor(getResources().getColor(android.R.color.black));
//            tvButton.setBackgroundResource(android.R.drawable.btn_default);
//
//        });
//
//
//        tvButton.setOnClickListener(view -> {
//            selectedProductButton = "TV";
//            tvButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//            tvButton.setTextColor(getResources().getColor(android.R.color.white));
//            d30Button.setTextColor(getResources().getColor(android.R.color.black));
//            d30Button.setBackgroundResource(android.R.drawable.btn_default);
//
//        });
//
//
//        followUpDate.setOnClickListener(v -> {
//            Calendar now = Calendar.getInstance();
//            DatePickerDialog dpd = DatePickerDialog.newInstance(AddTraineeActivity.this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
//            dpd.setVersion(DatePickerDialog.Version.VERSION_2);
//            dpd.setAccentColor(getResources().getColor(android.R.color.holo_orange_dark));
//            dpd.setOnCancelListener(dialogInterface -> {
//            });
//            dpd.show(getFragmentManager(), "Pick Follow Up Date");
//        });
//
//
//        yesExperience.setOnClickListener(view -> {
//            pegExperience = "YES";
//            yesExperience.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//            yesExperience.setTextColor(getResources().getColor(android.R.color.white));
//            noExperience.setTextColor(getResources().getColor(android.R.color.black));
//            noExperience.setBackgroundResource(android.R.drawable.btn_default);
//        });
//
//
//        noExperience.setOnClickListener(view -> {
//            pegExperience = "NO";
//            noExperience.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//            noExperience.setTextColor(getResources().getColor(android.R.color.white));
//            yesExperience.setTextColor(getResources().getColor(android.R.color.black));
//            yesExperience.setBackgroundResource(android.R.drawable.btn_default);
//        });
//
//
//        //AsyncTask.execute(this::getFusedLocationCoords);
//        //AsyncTask.execute(this::fetchAddressButtonHandler);

    }


    public void createRegionAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddTraineeActivity.this);
        builder.setTitle("Region");
        builder.setSingleChoiceItems(regionalValues, -1, (dialog, item) -> {
            selectedRegionValue = regionalValues[item] + "";
            region.setText(selectedRegionValue);
            alertDialog.dismiss();
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

//
//    private void getFusedLocationCoords() {
//        if (ActivityCompat.checkSelfPermission(AddTraineeActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
//        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(location -> {
//            if (location != null) {
//                mLastLocation = location;
//                locationValue.setText(location.toString());
//                coordinates.setText(String.format("%s,  %s", location.getLatitude(), location.getLongitude()));
//                //locationValue.setText(getAddress(AddTraineeActivity.this, location.getLatitude(), location.getLongitude()));
//            } else {
//                Toast.makeText(AddTraineeActivity.this, "Error", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
    }


    public String getAddress(Context context, double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "," + obj.getAdminArea();
            add = add + "," + obj.getCountryName();

            return add;

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return "N/A";
        }
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = +dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        followUpDate.setText(date);
    }


    private boolean validateFieldBounds(){
        return true;
    }


}
