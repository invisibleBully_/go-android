package activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import pegafrica.com.go_mobile.R;
import util.FontCache;

public class LoginActivity extends AppCompatActivity {



    private EditText emailEditText, passwordEditText;
    private AppCompatCheckBox rememberMeCheckBox;
    private Button signInButton;
    private Typeface typeface, typefaceBold;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        registerComponents();
    }








    private void registerComponents(){
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        typeface = FontCache.get("Lato-Regular.ttf", LoginActivity.this);
        typefaceBold = FontCache.get("Lato-Bold.ttf", LoginActivity.this);
        emailEditText = findViewById(R.id.email_address);
        passwordEditText = findViewById(R.id.password);
        rememberMeCheckBox = findViewById(R.id.remember_me);
        signInButton = findViewById(R.id.sign_in);
        emailEditText.setTypeface(typeface);
        passwordEditText.setTypeface(typeface);
        rememberMeCheckBox.setTypeface(typefaceBold);
        signInButton.setTypeface(typefaceBold);
        signInButton.setOnClickListener(view -> {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        });
    }




    public boolean validateFieldBounds(){
        return true;
    }


}
